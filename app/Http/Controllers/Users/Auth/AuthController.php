<?php

namespace App\Http\Controllers\Users\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\AgencyInformation;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class AuthController extends Controller
{
     // Login
    public function loginForm(){
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/users-pages/login',[
            'pageConfigs' => $pageConfigs
        ]);
        
    }

    // Register
    public function registerForm(){

        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/register',[
            'pageConfigs' => $pageConfigs
        ]);

       
  
      }
    public function makeRegister(Request $request){
        $status_msg=array();

        try{
          
            if($request->ajax()){
    
                $validator=Validator::make($request->all(), [
                    'firstname' => ['required', 'string', 'max:255'],
                    'lastname' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required_with:password_confirmation','confirmed','min:6'],
                    'user_type'=>['required','string'],
                    'business_name'=>['required','string','max:255'],
                    'owner_contact'=>['required','string','max:255'],
                    'address'=>['required','string','max:255'],
                    'licence'=>['required','string','max:255'],
                    'about_business'=>['required','string','max:255']
                ]);
        
               if($validator->passes()){
    
              
                $roles=[];
    
                $user_type_flag=1;
    
    
    
                $profile_key=rand();
    
                $user_type=$request->user_type;
    
                if($user_type==="hotel_business"){
                    $roles=['Super-Admin-Hotel','Editor-Hotel','Hotel-Receiptionist'];
           
                    //array_push($role_assign,$roles);
                    
                }
                else if($user_type==="vehicle_business"){
                    $roles= ['Super-Admin-Vehicle','Editor-Vehicle','Vehicle-Receiptionist'];
                    //array_push($role_assign,$roles);
                   
                }
                else if($user_type==="others"){
                    Permission::create(['name' => 'Other-Users']);
                    $roles=Role::create(['name' => 'Other-Users']);
                    $roles->givePermissionTo('Other-Users');
                }
    
                $user=[
                    'name'=>$request->firstname,
                    'email'=>$request->email,
                    'password'=>bcrypt($request->password),
                    'user_type'=>$request->user_type,
                    "user_access"=>json_encode([
                        "access_key"=>"1",
                        "access_name"=>$request->email
                        ])
                ];
        
                $business_info=[
                    'business_name'=>$request->business_name,
                    'owner_name'=>($request->firstname." ".$request->lastname),
                    'owner_email'=>$request->email,
                    'owner_contact'=>$request->owner_contact,
                    'address'=>$request->address,
                    'licence'=>$request->licence,
                    'about_business'=>$request->about_business
                ];
    
                    $agency_data=AgencyInformation::create($business_info);
                    $create_user=$agency_data->users()->create($user);
    
                    $role = Role::select('id')
                            ->whereIn('name',$roles)
                            ->get();
    
                    foreach ($role as $role_id) {
                        $role_r = Role::where('id', '=', $role_id->id)->firstOrFail();            
                        $create_user->assignRole($role_r); //Assigning role to user
                        }
    
                    $status_msg['status']=1;
                    echo json_encode($status_msg);
                
    
               }else{
                $status_msg['status']=2;
                $status_msg['errors']=$validator->errors()->all();
                echo json_encode($status_msg);
               }
    
            } 
           
        }catch(QueryException $e){
            $status_msg['status']=0;
            $status_msg['errors']=$e->getMessage();
            echo json_encode($status_msg);
        }
    }

    public function userView(){
        $user_info_id=auth()->user()->id;
        $user_id=auth()->user()->user_profile_id;
        $user_data=AgencyInformation::findOrFail($user_info_id);
        $user=User::findOrFail($user_id);
        echo  json_encode($user->getPermissionsViaRoles());
    }
    
    public function userLogout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/login');
    }


}
