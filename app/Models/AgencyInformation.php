<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyInformation extends Model
{

    protected $fillable=[
       'user_id',
       "business_name",
       'owner_name',
       'owner_email',
       'owner_contact',
       'address',
       'licence'
    ];

    public function users()
    {
        return $this->hasMany('App\User',"user_profile_id");
    } 
}
