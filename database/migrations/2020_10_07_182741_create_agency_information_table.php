<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("business_name");
            $table->string("owner_name");
            $table->string("owner_email");
            $table->string("owner_contact");
            $table->string("address");
            $table->string("licence");
            $table->string("about_business")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_information');
    }
}
