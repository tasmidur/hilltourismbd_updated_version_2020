<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class Administrator extends Seeder
{
    
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        //super admin permission
        Permission::create(['name' => 'Administer-roles-permissions']);//super admin permission
        //1)Editor of Super Admin 
        //(Role as Manage the all of booking and Accept request)
        Permission::create(['name' => 'Editor-Admin']);


        //Hotel permissions
        //Access all things
        Permission::create(['name' => 'Super-Admin-Hotel']); 
        //Can Modify and Add Properties
        //Can book the Room of Hotel
        Permission::create(['name' => 'Editor-Hotel']);
        //Only Can book the Room of Hotel
        Permission::create(['name' => 'Agent-Hotel']);


        //Vehicle Permission
        //Access all things
        Permission::create(['name' => 'Super-Admin-Vehicle']);
        //Can Modify and Add Properties
        //Can book the Vehicle
        Permission::create(['name' => 'Editor-Vehicle']);
        //Only Can book the Vehicle
        Permission::create(['name' => 'Agent-Vehicle']);
      
        // Admin Editor Role
        // gets all permissions via Gate::before rule; see AuthServiceProvider
        $role1 = Role::create(['name' => 'Super-Admin']);
        $role1->givePermissionTo('Administer-roles-permissions');
        $role1->givePermissionTo('Editor-Admin');
       //Admin Editor
        $role2 = Role::create(['name' => 'Admin-Editor']);
        
        $role2->givePermissionTo('Editor-Admin');

        //Hotel Editor Role
        $role3 = Role::create(['name' => 'Super-Admin-Hotel']);
        $role3->givePermissionTo('Super-Admin-Hotel'); 
        $role3->givePermissionTo('Editor-Hotel');  //Data can edit delete and update
        $role3->givePermissionTo('Agent-Hotel');   //A Editor can controll the hotel booking system

        //Can controll the hotel booking system
        $role4 = Role::create(['name' => 'Hotel-Receiptionist']);
        $role4->givePermissionTo('Agent-Hotel');

        //Can controll the hotel Editor
        $role5 = Role::create(['name' => 'Hotel-Editor']);
        $role5->givePermissionTo('Editor-Hotel');  //Data can edit delete and update
        $role5->givePermissionTo('Agent-Hotel');   //A Editor can controll the hotel booking system
    

         //Vehicle Super-Admin Role
        $role6 = Role::create(['name' => 'Super-Admin-Vehicle']);
        $role6->givePermissionTo('Super-Admin-Vehicle'); 
        $role6->givePermissionTo('Editor-Vehicle');  //Data can edit delete and update
        $role6->givePermissionTo('Agent-Vehicle');   //A Editor can controll the hotel booking system
        
        //Vehicel Editor role
        $role7 = Role::create(['name' => 'Vehicle-Editor']);
        $role7->givePermissionTo('Editor-Vehicle');//Data can edit delete and update
        $role7->givePermissionTo('Agent-Vehicle');
        
        $role8 = Role::create(['name' => 'Vehicle-Receiptionist']);
        $role8->givePermissionTo('Agent-Vehicle');

      

        // create demo users
        $user = \App\User::create([
            'name' => 'Super-Admin',
            "user_type"=>"super_admin",
            'email' => 'admin@admin.com',
            'password'=>bcrypt('123456'),
            "user_access"=>json_encode([
                "access_key"=>"1",
                "access_name"=>"Super-Admin"
                ])
        ]);

        $user->assignRole($role1);

        //php artisan db:seed --class=Administrator

        // $user = \App\User::create([
        //     'name' => 'Super-Admin-Hotel',
        //     "user_type"=>"hotel_business",
        //     'email' => 'hotel@hilltourismbd.com',
        //     'password'=>bcrypt('hoteladmin-123'),
        //     "user_access"=>json_encode([
        //         "access_key"=>"2",
        //         "access_name"=>"Super-Admin-Hotel"
        //         ])
        // ]);

        // $user->assignRole($role3);

        // $user = \App\User::create([
        //     'name' => 'Super-Admin-Vehicle',
        //     "user_type"=>"vehicle_business",
        //     'email' => 'vehicle@hilltourismbd.com',
        //     'password'=>bcrypt('vehicle-admin-123')
            
        // ]);
        // $user->assignRole($role6);
    }


}
