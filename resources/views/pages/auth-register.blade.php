
@extends('layouts/fullLayoutMaster')

@section('title', 'Register Page')

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
        <!-- <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}"> -->
@endsection
@section('content')

<!-- Form wizard with step validation section start -->
<section id="validation row flexbox-container">
    <div class="row">
        <div class="col-12 p-5">
            <div class="card">
                <div class="card-header" style="margin-left:0px;margin-right:0px">
                    <h4 class="card-title">Register</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form  class="steps-validation wizard-circle" id="registerform" enctype="multipart/form-data">
                            <!-- Step 1 -->
                            <h6><i class="step-icon feather icon-user"></i>Authentication</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="location">
                                            Register As
                                            </label>
                                            <select class="custom-select form-control" id="user-type" name="user-type">
                                                <option value="">User Type</option>
                                                <option value="hotel-agent">Hotel Agent</option>
                                                <option value="Vehicle-agent">Vehicle Agent</option>
                                                <option value="others">Others</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                Email
                                            </label>
                                            <input type="email" value="demo@gamil.com" class="form-control required" id="email" name="email" >
                                        </div>
                                    </div>

                                   
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="emailAddress5">
                                                User Name
                                            </label>
                                            <input type="text" value="User" class="form-control required" id="username" name="username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lastName3">
                                                Password
                                            </label>
                                            <input type="password" value="12345678" class="form-control required" id="lastName3" name="lastName">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <!-- Step 2 -->
                            <h6><i class="step-icon feather icon-briefcase"></i>Business Information</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="proposalTitle3">
                                            Business Name
                                            </label>
                                            <input type="text" value="Hotel" class="form-control required" id="businessname" name="businessname">
                                        </div>
                                        <div class="form-group">
                                            <label for="proposalTitle3">
                                             Owner Name
                                            </label>
                                            <input type="text" value="Piyal Hasan" class="form-control required" id="ownername" name="ownername">
                                        </div>

                                        <div class="form-group">
                                                <label for="jobTitle5">
                                                Owner Contact
                                                </label>
                                                <input type="text" value="0176711434" class="form-control required" id="ownercontact" name="ownercontact" >
                                        </div>

                                        <div class="form-group">
                                            <label for="jobTitle5">
                                            Owner Email
                                            </label>
                                            <input type="email" value="piyalmbstu@gmail.com" class="form-control" id="owneremail" name="owneremail" >
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                                <label for="jobTitle5">
                                                Address
                                                </label>
                                                <input type="text" value="Dhaka, Bangladesh" class="form-control required" id="address" name="address" >
                                        </div>
                                        <div class="form-group">
                                                <label for="jobTitle5">
                                                Business Licence
                                                </label>
                                                <input type="text" value="12222112112" class="form-control" id="licence" name="licence" >
                                        </div>
                                        <div class="form-group">
                                            <label for="shortDescription3">Short Description of Your Business</label>
                                            <textarea name="about_business" id="about_business" rows="4" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Form wizard with step validation section end -->
@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/forms/wizard-steps.js')) }}"></script>
@endsection
